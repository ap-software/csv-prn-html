package com.ap_software.csv_prn_html.browser;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Stage;



@SuppressWarnings("restriction")
public class Browser extends Application {  
   
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("csv_prn_html");

        WebView webView = new WebView();
        WebEngine webEngine = webView.getEngine();

        String url = "http://localhost:8080";
        webEngine.load(url);

        StackPane sp = new StackPane();
        sp.getChildren().add(webView);

        Scene root = new Scene(sp);

        stage.setScene(root);
        stage.show();
        
        stage.setOnCloseRequest(event -> {
        	Platform.exit();
        	System.exit(0);
        });
    }
    
}