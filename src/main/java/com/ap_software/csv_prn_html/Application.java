package com.ap_software.csv_prn_html;

import org.springframework.boot.*;
import org.springframework.boot.autoconfigure.*;
import org.springframework.stereotype.*;
import org.springframework.web.bind.annotation.*;

import com.ap_software.csv_prn_html.browser.Browser;

@Controller
@EnableAutoConfiguration
@SpringBootApplication
public class Application{

    @RequestMapping("/")
    //@ResponseBody
    String home() {
        return "redirect:upload.html";
    }

    @SuppressWarnings("restriction")
	public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
        
        Browser.launch(Browser.class, args);
        
    }
}