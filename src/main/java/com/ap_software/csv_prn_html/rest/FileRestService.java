package com.ap_software.csv_prn_html.rest;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import com.ap_software.csv_prn_html.parser.Parser;
import com.ap_software.csv_prn_html.parser.factory.ParserFactory;
import com.ap_software.csv_prn_html.parser.factory.impl.ParserFactoryImpl;

@RestController
@RequestMapping("/api")
public class FileRestService {
	
	
	@RequestMapping(value="/test", method=RequestMethod.GET)
	public ResponseEntity<?> test() {
        return new ResponseEntity<String>("test", HttpStatus.OK);
	}
	
	@RequestMapping(value="/upload", method= RequestMethod.POST)
	public ResponseEntity<?> upload(@RequestParam("file") MultipartFile file, RedirectAttributes redirectAttributes) {
		ParserFactory parserFactory = new ParserFactoryImpl();
		Parser parser = parserFactory.createParser(file);
        return  new ResponseEntity<String>(parser.toHtml(file), HttpStatus.OK);
	}
}
