package com.ap_software.csv_prn_html.parser.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import org.apache.any23.encoding.TikaEncodingDetector;
import org.springframework.web.multipart.MultipartFile;

import com.ap_software.csv_prn_html.parser.Parser;

public class PrnParser implements Parser {
		
	List<List<String>> parsedContents = new ArrayList<List<String>>();
	
	@Override
	public List<List<String>> parse(MultipartFile file) {
		
		Charset charset;
		try {
			charset = Charset.forName(new TikaEncodingDetector().guessEncoding(file.getInputStream()));
		} catch (IOException e) {
			e.printStackTrace();
			return new ArrayList<List<String>>();
		}
		try (BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(file.getInputStream(),charset))){
			bufferedReader.lines().forEach(line -> {
				List<String> row = new ArrayList<String>();
				parsedContents.add(row);
				//row.add(line.replaceAll(" ", Parser.NON_BREAKING_SPACE));
				row.add(line);
			});
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return parsedContents;
	}
	
	@Override
	public String toHtml(MultipartFile file) {
//		StringBuffer styledTableStartTag = new StringBuffer("<table style=\"font-family: monospace;\">");
//		this.parse(file);
//		StringBuffer strBuffer = new StringBuffer();
//		strBuffer.append(styledTableStartTag).append("\n");
//		parsedContents.forEach(record -> {
//			strBuffer.append("\t").append(TR_START_TAG).append("\n");
//			record.forEach(element -> strBuffer.append("\t\t").append(TD_START_TAG).append(element).append(TD_END_TAG).append("\n"));
//			strBuffer.append("\t").append(TR_END_TAG).append("\n");
//			});
//		strBuffer.append(TABLE_END_TAG);
		StringBuffer styledDivStartTag = new StringBuffer("<div style=\"font-family: monospace;\">");
		this.parse(file);
		StringBuffer strBuffer = new StringBuffer();
		strBuffer.append(styledDivStartTag).append("\n");
		parsedContents.forEach(record -> {
			record.forEach(element -> strBuffer.append("\t")
					.append(element.replaceAll(" ", Parser.NON_BREAKING_SPACE))
					.append(BR_END_TAG).append("\n"));
		});
		strBuffer.append(DIV_END_TAG);
		return strBuffer.toString();
	}
}
