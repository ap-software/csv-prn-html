package com.ap_software.csv_prn_html.parser;

import java.util.List;
import org.springframework.web.multipart.MultipartFile;

public interface Parser {
	
	public static final String TABLE_START_TAG = "<table>";
	public static final String TABLE_END_TAG = "</table>";
	public static final String TH_START_TAG = "<th>";
	public static final String TH_END_TAG = "</th>";
	public static final String TR_START_TAG = "<tr>";
	public static final String TR_END_TAG = "</tr>";
	public static final String TD_START_TAG = "<td>";
	public static final String TD_END_TAG = "</td>";
	
	public static final String 	BR_START_TAG = "<br>";
	public static final String 	BR_END_TAG = "</br>";
	
	public static final String 	DIV_START_TAG = "<div>";
	public static final String 	DIV_END_TAG = "</div>";
	
	public static final String 	NON_BREAKING_SPACE = "&nbsp;";
	
	public List<List<String>> parse(MultipartFile file);
	public String toHtml(MultipartFile file);

}
