package com.ap_software.csv_prn_html.parser.factory.impl;

import org.apache.commons.io.FilenameUtils;
import org.springframework.web.multipart.MultipartFile;

import com.ap_software.csv_prn_html.parser.Parser;
import com.ap_software.csv_prn_html.parser.factory.ParserFactory;
import com.ap_software.csv_prn_html.parser.impl.CsvParser;
import com.ap_software.csv_prn_html.parser.impl.PrnParser;

public class ParserFactoryImpl implements ParserFactory {

	@Override
	public Parser createParser(MultipartFile file){
		if(file==null) {
			return null;
		}
		String  fileExtension = FilenameUtils.getExtension(file.getOriginalFilename());
		
		if(fileExtension.equals("csv")) {
			return new CsvParser();
		} else if (fileExtension.equals("prn")) {
			return new PrnParser();
		} else {
			return null;
		}
	}

}
