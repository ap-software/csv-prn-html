package com.ap_software.csv_prn_html.parser.factory;

import org.springframework.web.multipart.MultipartFile;

import com.ap_software.csv_prn_html.parser.Parser;

public interface ParserFactory {

	public Parser createParser(MultipartFile file);
	
}
