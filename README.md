# Track

This project was created to convert CSV and PRN files to HTML

## Technology

Spring Boot

## Compilation

mvn clean install

## Run

Please use Java 8:<br />
java -jar csv-prn-html.jar

## Further help

apoklade@gmail.com